# READ ME

This directory contains the source code for [Tool Labels Plugin for Hudson][].
It allows users to add labels dynamically to every node with a specific
tool installation so that a job can restrict the nodes where it runs by their
tool locations.

This program also has another edition [for Jenkins][].

[Tool Labels Plugin for Hudson]: <http://kazssym.bitbucket.org/hudson-tool-labels-plugin/>
[For Jenkins]: <https://bitbucket.org/kazssym/jenkins-tool-labels-plugin>

## License

This program is *[free software][]*: you can redistribute it and/or modify it
under the terms of the *[GNU Affero General Public License][]* as published by
the [Free Software Foundation][], either version 3 of the License, or (at your
option) any later version.

You should be able to receive a copy of the GNU Affero General Public License
along with this program.

[Free software]: <http://www.gnu.org/philosophy/free-sw.html> "What is free software?"
[GNU Affero General Public License]: <http://www.gnu.org/licenses/agpl.html>
[Free Software Foundation]: <http://www.fsf.org/>
