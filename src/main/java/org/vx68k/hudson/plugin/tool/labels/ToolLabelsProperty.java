/*
 * ToolLabelsProperty
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.vx68k.hudson.plugin.tool.labels;

import hudson.Extension;
import hudson.tools.ToolInstallation;
import hudson.tools.ToolProperty;
import hudson.tools.ToolPropertyDescriptor;
import org.kohsuke.stapler.DataBoundConstructor;
import org.vx68k.hudson.plugin.tool.labels.resources.Messages;

/**
 *
 * @author Kaz Nishimura
 * @since 2.0
 */
public class ToolLabelsProperty extends ToolProperty<ToolInstallation> {

    /**
     * Returns the {@link ToolLabelsProperty} object of a tool installation.
     *
     * @param installation tool installation
     * @return {@link ToolLabelsProperty} object, or <code>null</code> if not
     * found.
     * @since 3.0
     */
    public static ToolLabelsProperty of(ToolInstallation installation) {
        for (ToolProperty<?> p : installation.getProperties()) {
            if (p instanceof ToolLabelsProperty) {
                return (ToolLabelsProperty) p;
            }
        }
        return null;
    }

    private final String labels;

    @DataBoundConstructor
    public ToolLabelsProperty(String labels) {
        this.labels = labels;
    }

    /**
     * Returns a {@link String} object of labels.
     * @return {@link String} object of labels.
     */
    public String getLabels() {
        return labels;
    }

    /**
     * Returns a {@link Class} object for {@link ToolInstallation}.
     *
     * @return {@link Class} object for {@link ToolInstallation}
     */
    @Override
    public Class type() {
        return ToolInstallation.class;
    }

    /**
     * Describes {@link ToolLabelsProperty}.
     *
     * @author Kaz Nishimura
     * @since 3.0
     */
    @Extension
    public static final class Descriptor extends ToolPropertyDescriptor {

        /**
         * Returns the display name for {@link ToolLabelsProperty}.
         *
         * @return display name for {@link ToolLabelsProperty}
         */
        @Override
        public String getDisplayName() {
            return Messages.getToolPropertyDisplayName();
        }
    }
}
