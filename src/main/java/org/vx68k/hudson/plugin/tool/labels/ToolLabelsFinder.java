/*
 * ToolLabelsFinder
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.vx68k.hudson.plugin.tool.labels;

import java.util.Collection;
import java.util.HashSet;
import java.util.StringTokenizer;
import hudson.Extension;
import hudson.model.Descriptor;
import hudson.model.Hudson;
import hudson.model.LabelFinder;
import hudson.model.Node;
import hudson.model.labels.LabelAtom;
import hudson.tools.ToolDescriptor;
import hudson.tools.ToolInstallation;
import hudson.tools.ToolLocationNodeProperty;

/**
 * Finds labels that shall be added dynamically to a node with
 * all specified tool installations.
 *
 * @author Kaz Nishimura
 * @since 2.0
 */
@Extension
public class ToolLabelsFinder extends LabelFinder {

    /**
     * Returns the {@link ToolLabelsProperty} object of a tool installation.

     * @param installation tool installation
     * @return {@link ToolLabelsProperty} object, or <code>null</code> if not
     * found.
     * @deprecated As of version 3.0, replaced by {@link
     * ToolLabelsProperty#of}
     */
    @Deprecated
    protected ToolLabelsProperty getToolLabelsProperty(
            ToolInstallation installation) {
        return ToolLabelsProperty.of(installation);
    }

    /**
     * Adds labels for all specified tool locations.
     *
     * @param node node where tool locations are checked
     * @param installations tool installations
     * @param labels collection to which labels are added
     */
    protected void addLabels(Node node, ToolInstallation[] installations,
            Collection<LabelAtom> labels) {
        ToolLocationNodeProperty locations = node.getNodeProperties()
                .get(ToolLocationNodeProperty.class);

        Hudson application = Hudson.getInstance();
        for (ToolInstallation i : installations) {
            String home = null;
            if (locations != null) {
                home = locations.getHome(i);
            }
            if (home == null) {
                home = "";
                if (node instanceof Hudson) {
                    // Only for master node:
                    home = i.getHome();
                    assert home != null;
                }
            }
            if (!home.isEmpty()) {
                ToolLabelsProperty property = ToolLabelsProperty.of(i);
                if (property != null) {
                    StringTokenizer tokenizer
                            = new StringTokenizer(property.getLabels());
                    while (tokenizer.hasMoreTokens()) {
                        String token = tokenizer.nextToken();
                        labels.add(application.getLabelAtom(token));
                    }
                }
            }
        }
    }

    /**
     * Finds labels that shall be added dynamically to a node with
     * all specified tool installations.
     *
     * @param node node to which labels shall be added
     * @return collection of all labels that shall be added
     */
    @Override
    public Collection<LabelAtom> findLabels(Node node) {
        Hudson application = Hudson.getInstance();

        HashSet<LabelAtom> labels = new HashSet();
        for (Descriptor<ToolInstallation> d :
                application.getDescriptorList(ToolInstallation.class)) {
            if (d instanceof ToolDescriptor<?>) {
                ToolDescriptor<?> descriptor = (ToolDescriptor<?>) d;
                addLabels(node, descriptor.getInstallations(), labels);
            }
        }
        return labels;
    }
}
