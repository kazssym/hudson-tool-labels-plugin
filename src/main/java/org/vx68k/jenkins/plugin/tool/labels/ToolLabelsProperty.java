/*
 * ToolLabelsProperty for backward compatibility
 * Copyright (C) 2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.vx68k.jenkins.plugin.tool.labels;

import hudson.Extension;
import hudson.tools.ToolInstallation;
import hudson.tools.ToolProperty;
import hudson.tools.ToolPropertyDescriptor;
import hudson.util.XStream2;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import org.kohsuke.stapler.DataBoundConstructor;
import org.vx68k.hudson.plugin.tool.labels.resources.Messages;

/**
 *
 * @author Kaz Nishimura
 * @since 1.0
 * @deprecated As of version 2.0, replaced by {@link
 * org.vx68k.hudson.plugin.tool.labels.ToolLabelsProperty}.
 */
@Deprecated
public class ToolLabelsProperty extends ToolProperty<ToolInstallation> {

    private final String labels;

    @DataBoundConstructor
    public ToolLabelsProperty(String labels) {
        this.labels = labels;
    }

    /**
     * Returns a {@link String} object of labels.
     * @return {@link String} object of labels.
     */
    public String getLabels() {
        return labels;
    }

    /**
     * Returns a {@link Class} object for {@link ToolInstallation}.
     *
     * @return {@link Class} object for {@link ToolInstallation}
     */
    @Override
    public Class<ToolInstallation> type() {
        return ToolInstallation.class;
    }

    /**
     * Converter to unmarshal old data into the new class.
     *
     * @since 2.0
     */
    public static final class ConverterImpl
            extends XStream2.PassthruConverter<ToolLabelsProperty> {

        public ConverterImpl(XStream2 xstream) {
            super(xstream);
        }

        @Override
        public Object unmarshal(HierarchicalStreamReader reader,
                UnmarshallingContext context) {
            Object object = super.unmarshal(reader, context);
            if (object instanceof ToolLabelsProperty) {
                String labels = ((ToolLabelsProperty) object).getLabels();
                object = new org.vx68k.hudson.plugin.tool.labels
                        .ToolLabelsProperty(labels);
            }
            return object;
        }

        /**
         * Does nothing.
         *
         * @param object deprecated {@link ToolLabelsProperty} object
         * @param context {@link UnmarshallingContext} object
         */
        @Override
        protected void callback(ToolLabelsProperty object,
                UnmarshallingContext context) {
        }
    }

    /**
     * Describes deprecated {@link ToolLabelsProperty}.
     *
     * @author Kaz Nishimura
     * @since 3.0
     */
    @Extension
    public static final class Descriptor extends ToolPropertyDescriptor {

        /**
         * Returns <code>false</code> to make this property hidden from
         * users.
         *
         * @param type a {@link Class} object for tool installations
         * @return <code>false</code>
         */
        @Override
        public boolean isApplicable(Class<? extends ToolInstallation> type) {
            return false;
        }

        /**
         * Returns the display name for {@link ToolLabelsProperty}.
         *
         * @return display name for {@link ToolLabelsProperty}
         */
        @Override
        public String getDisplayName() {
            return Messages.getToolPropertyDisplayName();
        }
    }
}
