/*
 * ToolLabelsPropertyTest
 * Copyright (C) 2014-2015 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.vx68k.hudson.plugin.tool.labels;

import hudson.tools.ToolInstallation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.vx68k.hudson.plugin.tool.labels.resources.Messages;

/**
 * Test for {@link ToolLabelsProperty}.
 *
 * @author Kaz Nishimura
 * @since 3.0
 */
public class ToolLabelsPropertyTest {

    private ToolLabelsProperty.Descriptor descriptor;

    @Before
    public void setUp() {
        descriptor = new ToolLabelsProperty.Descriptor();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testToolLabelsPropertyLabels() {
        final String TEST_LABELS = "label1 label2";
        ToolLabelsProperty property = new ToolLabelsProperty(TEST_LABELS);
        assertEquals(TEST_LABELS, property.getLabels());
    }

    @Test
    public void testToolLabelsPropertyType() {
        ToolLabelsProperty property = new ToolLabelsProperty("");
        assertEquals(ToolInstallation.class, property.type());
    }

    @Test
    public void testToolLabelsPropertyDescriptorApplicable() {
        assertTrue(descriptor.isApplicable(ToolInstallation.class));
    }

    @Test
    public void testToolLabelsPropertyDescriptorDisplayName() {
        assertEquals(Messages.getToolPropertyDisplayName(),
                descriptor.getDisplayName());
    }
}
